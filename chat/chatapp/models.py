from datetime import timezone
import uuid

from django.db import models
from django.contrib.auth.models import AbstractUser



class User(AbstractUser):
    last_read_date = models.DateTimeField(
        auto_now_add=True,
        blank=False,
        null=False
    )
    online = models.BooleanField(null=False, blank=False, default=False)

    REQUIRED_FIELDS = []

    def __str__(self):
        return self.username

    def read(self):
        self.last_read_date = timezone.now()
        self.save()

    def unread_messages(self):
        return Message.objects.filter(created_at__gt=self.last_read_date) \
            .count()


class Message(models.Model):
    id = models.UUIDField(null=False, default=uuid.uuid4)

    author = models.ForeignKey('User', related_name='author_messages', primary_key=True,  null=False, on_delete=models.CASCADE)
    content = models.TextField(max_length=50, null=False)
    created_at = models.DateTimeField(auto_now_add=True, blank=False)

    @staticmethod
    def last_messages():
        return Message.objects.order_by('-created_at').all()[:50]  # get last 50 messages
